#!/usr/bin/env bash

# stop all containers
docker-compose down

# copy database 
cp ./build/db/init/${COMPOSE_PROJECT_NAME}_bkp.sql ./build/db/init/${COMPOSE_PROJECT_NAME}.sql

# copy configuration file
cp ./build/apache/docker-configuration.php ./src/configuration.php

# change file permissions
sudo chmod -R 777 ./src/

# start containers and build images
docker-compose up --build
