#!/usr/bin/env bash

FILE=.env

[ -f ${FILE} ] && {  
    echo "$FILE already exists."
    exit
}

read -p "Project name: " PROJECT_NAME
read -p "Project URL [${PROJECT_NAME}.devlocal]: " url
read -p "Public folder [public]: " folder
URL=${url:-${PROJECT_NAME}.devlocal}
# folder=${folder:-public}

cp -n .env.example ${FILE}
sed -i "s|project|${PROJECT_NAME}|g" $FILE
sed -i "s|local.devlocal|$URL|g" $FILE
# sed -i "" "s|public|$folder|g" $FILE

echo Project configured
echo name: $PROJECT_NAME
# echo folder: $FOLDER
echo URL: https://$URL
