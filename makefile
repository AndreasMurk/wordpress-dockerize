SHELL := /usr/bin/env bash
SCRIPTS := ./scripts

.PHONY: run build default clean

default: run build

build:
	$(SHELL) $(SCRIPTS)/build.sh
destroy:
	$(SHELL) $(SCRIPTS)/destroy.sh
run:
	docker-compose up
logs:
	docker-compose logs -f
clean:
	docker image prune -af && \
	docker network prune -f && \
	docker rm $(docker ps -q)
